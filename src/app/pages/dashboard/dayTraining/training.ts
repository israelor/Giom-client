export interface Training {
  time: Date;
  trainer: string;
  maxUsers: number;
  orders: number;
  location: string;
  userRegister: boolean;
}

export interface DayTraining {
  date: Date;
  trainings: Training[];
}

