import {Component, OnDestroy, Input} from '@angular/core';
import {NbThemeService} from '@nebular/theme';

import {ElectricityService} from '../../../@core/data/electricity.service';
import {DayTraining} from './training';

@Component({
  selector: 'ngx-day-training',
  styleUrls: ['./dayTraining.component.scss'],
  templateUrl: './dayTraining.component.html',
})
export class DayTrainingComponent implements OnDestroy {
  data: Array<any>;
  @Input() day: DayTraining;
  currentTheme: string;
  themeSubscription: any;

  constructor(private eService: ElectricityService, private themeService: NbThemeService) {
    this.data = this.eService.getData();

    this.themeSubscription = this.themeService.getJsTheme().subscribe(theme => {
      this.currentTheme = theme.name;
    });
  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
  }
}
