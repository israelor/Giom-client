import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss'],
})
export class ClockComponent implements OnInit {

  constructor() { }
  now = new Date();

    ngOnInit() {
      const timeoutId = setInterval(() => {
        this.now = new Date();
      }, 1000);
    }
}
