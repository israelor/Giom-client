import { Input, NgModule } from '@angular/core';
import { AngularEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { DayTrainingComponent } from './dayTraining/dayTraining.component';
import { StatusCardComponent } from './status-card/status-card.component';
import { PickDateComponent } from './pick-date/pick-date.component';
import { WeatherComponent } from './weather/weather.component';
import { ClockComponent } from './clock/clock.component';

@NgModule({
  imports: [
    ThemeModule,
    AngularEchartsModule,
  ],
  declarations: [
    DashboardComponent,
    DayTrainingComponent,
    StatusCardComponent,
    PickDateComponent,
    WeatherComponent,
    ClockComponent,
  ],
})
export class DashboardModule {
}
