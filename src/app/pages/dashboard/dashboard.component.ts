import { Component } from '@angular/core';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent {
  training = [{
    time: new Date(),
    orders: 4,
    maxUsers: 12,
    trainer: 'Barak Tenenboim',
    location: 'Central Park',
  },
    {
      time: new Date(),
      orders: 4,
      maxUsers: 12,
      trainer: 'Barak Tenenboim',
      location: 'Central Park',
    },
    {
      time: new Date(),
      orders: 4,
      maxUsers: 12,
      trainer: 'Barak Tenenboim',
      location: 'Central Park',
    },
    {
      time: new Date(),
      orders: 4,
      maxUsers: 12,
      trainer: 'Barak Tenenboim',
      location: 'Central Park',
    },
    {
      time: new Date(),
      orders: 12,
      maxUsers: 12,
      trainer: 'Barak Tenenboim',
      location: 'Central Park',
    },
    {
      time: new Date(),
      orders: 10,
      maxUsers: 12,
      trainer: 'Barak Tenenboim',
      location: 'Central Park',
      userRegister: true,
    }];
  day = [{
    date: new Date(),
    trainings: this.training,
  },
    {
      date: new Date(new Date().setDate(new Date().getDate() + 1)),
      trainings: this.training,
    },
    {
      date: new Date(new Date().setDate(new Date().getDate() + 2)),
      trainings: this.training,
    },
    {
      date: new Date(new Date().setDate(new Date().getDate() + 3)),
      trainings: this.training,
    },
  ];

  constructor() {
  }
}
