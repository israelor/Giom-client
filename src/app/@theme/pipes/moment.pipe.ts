import { Pipe, PipeTransform } from '@angular/core';
import * as Moment from 'moment';

@Pipe({
  name: 'moment',
})
export class MomentPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value) return '';
    return Moment(value).format(args);
  }

}
